# Rofmoji

Quickly find Emoji with Rofi! 😎

![Rofmoji](./media/rofmoji.gif)

## Install

Available on the AUR: [rofmoji](https://aur.archlinux.org/packages/rofmoji/)

```shell
yay -S rofmoji
```

## Usage

### 1. Bind `rofmoji` to a key! 🥳

```
bindsym $mod+i exec rofmoji
```
### 2. Type keywords to search for an Emoji

Emoji Modifiers are also available, Include one or more of the following in your query:

#### Skin tone
`light, medium-light, medium, medium-dark, dark`

#### Gender
`man, woman, gender-neutral`

#### Hair
`red, curly, blonde, white, bald`

### Examples

- 👱🏾‍♂ - `medium-dark blond man`
- 👩🏿‍⚕ - `dark woman doctor`
- 👨🏾‍🚒 - `dark firefighter man`
- 👩🏻‍🦰 - `light red woman`
- 🧑🏾 - `gender-neutral dark adult`
- 🧙🏽 - `witch medium`
- 👮‍♀ - `police woman`
- 👨‍💻 - `coder man`
- 👌 - `ok hand`
- 🐸 - `frog`
- 🤯 - `exploding head`


## Dependencies
- `rofi`
- `libnotify`
- `wl-clipboard (optional, prioritized)`
- `xclip (optional)`

## Contributing

If you have a suggestion to improve `rofmoji`, such as Emoji keywords, please make a Pull Request.
